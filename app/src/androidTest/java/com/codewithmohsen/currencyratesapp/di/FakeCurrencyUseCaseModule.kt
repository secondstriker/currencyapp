package com.codewithmohsen.currencyratesapp.di

import com.codewithmohsen.currencyratesapp.core.domain.UiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.data.di.CurrencyRepositoryModule
import com.codewithmohsen.currencyratesapp.domain.repository.CurrencyRepository
import com.codewithmohsen.currencyratesapp.domain.uiModel.ExchangeSymbol
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiRate
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.flow.flow
import javax.inject.Singleton

@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [CurrencyRepositoryModule::class]
)
@Module
object FakeCurrencyUseCaseModule {

    @Singleton
    @Provides
    fun provideCurrencyRepository(): CurrencyRepository = object : CurrencyRepository {
        override fun getCurrency(): UiStateFlow<UiCurrencyRates> =
            flow {
                emit(
                    UiState(
                        data = UiCurrencyRates(
                            uiRates = listOf(
                                UiRate(
                                    price = 1.0,
                                    symbol = ExchangeSymbol.EURUSD,
                                    slope = Slope.Positive
                                )
                            ), lastUpdate = ""
                        ),
                        loading = false,
                        throwable = null
                    )
                )
            }
    }
}