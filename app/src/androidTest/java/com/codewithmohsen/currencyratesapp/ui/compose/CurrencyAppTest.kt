package com.codewithmohsen.currencyratesapp.ui.compose

import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.codewithmohsen.currencyratesapp.MainActivity
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme
import com.codewithmohsen.currencyratesapp.presentation.ui.screen.CurrencyScreen
import com.codewithmohsen.currencyratesapp.presentation.vm.CurrencyViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@HiltAndroidTest
@OptIn(ExperimentalMaterial3Api::class)
class CurrencyAppTest {

    @get:Rule(order = 1)
    var hiltTestRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    var rule = createAndroidComposeRule<MainActivity>()

    private lateinit var viewModel: CurrencyViewModel

    @Before
    fun setup() {
        hiltTestRule.inject()

        viewModel = rule.activity.viewModels<CurrencyViewModel>().value

        rule.activity.setContent {
            CurrencyRatesAppTheme() {
                CurrencyScreen(viewModel)
            }
        }
    }

    @Test
    fun when_screen_is_loaded() {
        rule.onNodeWithText("EUR/USD").assertExists()
        rule.onNodeWithText("1.0").assertExists()
        rule.onNodeWithText("invalid-data").assertDoesNotExist()
        rule.onNodeWithText("no-connectivity").assertDoesNotExist()
    }
}