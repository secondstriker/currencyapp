package com.codewithmohsen.currencyratesapp.utils

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Rule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.flow.toCollection
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class NetworkExtTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `happy path`() = runTest {
        val result = "Success"
        val happyFlow = flow<String> {
            emit(result)
        }

        val responses: MutableList<BaseResponse<String>> = mutableListOf()

        happyFlow.toResult().toCollection(responses)
        assertTrue(responses[0] is BaseResponse.Loading)
        assertTrue(responses[1] is BaseResponse.Success)
        assertTrue((responses[1] as BaseResponse.Success).data == result)
    }

    @Test
    fun `not happy path`() = runTest {
        val error = "Error"
        val happyFlow = flow<String> {
            throw Exception(error)
        }

        val responses: MutableList<BaseResponse<String>> = mutableListOf()

        happyFlow.toResult().toCollection(responses)
        assertTrue(responses[0] is BaseResponse.Loading)
        assertTrue(responses[1] is BaseResponse.Error)
        assertTrue((responses[1] as BaseResponse.Error).error == error)
    }


}