package com.codewithmohsen.currencyratesapp.utils

import com.codewithmohsen.currencyratesapp.utils.BaseResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

fun <T> Flow<T>.toResult(): Flow<com.codewithmohsen.currencyratesapp.utils.BaseResponse<T>> = this
    .map<T, com.codewithmohsen.currencyratesapp.utils.BaseResponse<T>> {
        com.codewithmohsen.currencyratesapp.utils.BaseResponse.Success(it)
    }.onStart {
        emit(com.codewithmohsen.currencyratesapp.utils.BaseResponse.Loading())
    }.catch {
        emit(com.codewithmohsen.currencyratesapp.utils.BaseResponse.Error(it.message))
    }