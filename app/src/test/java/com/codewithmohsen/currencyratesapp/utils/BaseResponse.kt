package com.codewithmohsen.currencyratesapp.utils

sealed class BaseResponse<out T> {

    data class Success<out T>(val data: T?) : BaseResponse<T>()

    data class Error<T>(val error: String?) : BaseResponse<T>()

    class Loading<T> : BaseResponse<T>()
}
