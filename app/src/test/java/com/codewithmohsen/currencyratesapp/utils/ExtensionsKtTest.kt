package com.codewithmohsen.currencyratesapp.utils

import com.codewithmohsen.currencyratesapp.core.data.remote.DTO
import com.codewithmohsen.currencyratesapp.core.data.remote.RemoteResponse
import com.codewithmohsen.currencyratesapp.core.data.remote.remoteResponseToUiState
import com.codewithmohsen.currencyratesapp.core.domain.UiData
import com.codewithmohsen.currencyratesapp.core.domain.UiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*

import org.junit.Test

//example of Dto
data class MyDto(val name: String) : DTO

//example of UiData
data class MyUiData(val title: String) : UiData

class ExtensionsKtTest {

    private val serverData = flow {
        emit(RemoteResponse(MyDto("mohsen"), true, null))
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun toResult() = runTest {
        val uiData: UiStateFlow<MyUiData> =
            serverData.remoteResponseToUiState(mapper = { dto -> MyUiData(title = dto.name) })
        val uiDataList = mutableListOf<UiState<MyUiData>>()
        uiData.toCollection(uiDataList)

        assert(uiDataList[0].loading)
        assert(!uiDataList[1].loading)
        assert(uiDataList[1].data != null)
        assert(uiDataList[1].data?.title == "mohsen")
    }
}