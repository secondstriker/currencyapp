package com.codewithmohsen.currencyratesapp.utils

import org.junit.Assert.*

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class NumUtilsTest {

    @Test
    fun `roundTo 563634235632623`() {
        val x = 0.563634235632623
        val rounded = x.roundTo(2)
        assertEquals(0.56, rounded, 0.0)
    }

    @Test
    fun `roundTo 0`() {
        val x = 0.0
        val rounded = x.roundTo(2)
        assertEquals(0.0, rounded, 0.0)
    }

    @Test
    fun `roundTo 995`() {
        val x = 0.999
        val rounded = x.roundTo(2)
        assertEquals(1.00, rounded, 0.0)
    }

    @Test
    fun `roundTo 504`() {
        val x = 0.504
        val rounded = x.roundTo(2)
        assertEquals(0.50, rounded, 0.0)
    }
}