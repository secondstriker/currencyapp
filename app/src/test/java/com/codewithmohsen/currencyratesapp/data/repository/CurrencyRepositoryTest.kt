package com.codewithmohsen.currencyratesapp.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.codewithmohsen.currencyratesapp.data.remote.CurrencyRates
import com.codewithmohsen.currencyratesapp.data.remote.Rate
import com.codewithmohsen.currencyratesapp.data.remote.RatesService
import org.junit.Rule

class CurrencyRepositoryTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val currencyRates = CurrencyRates(
        rates = listOf(
            Rate(price = 1.0, symbol = "USDEUR")
        )
    )

    private val successResponseService: RatesService = object : RatesService {
        override suspend fun loadRates(): CurrencyRates {
            return currencyRates
        }
    }

    //we test just getting data,
    // error and loading obviously should be tested for the extension toResult
//    @OptIn(ExperimentalCoroutinesApi::class)
//    @Test
//    fun `getCurrency when it is success`() =
//        runTest {
//
//            val repo = CurrencyRepositoryImpl(successResponseService)
//
//            val result = repo.getCurrency(null)
//
//            val responses: MutableList<UiState<UiCurrencyRates>> = mutableListOf()
//            result.toCollection(responses)
//
//            assertEquals(currencyRates.toUi(null), (responses[1].data))
//        }
}