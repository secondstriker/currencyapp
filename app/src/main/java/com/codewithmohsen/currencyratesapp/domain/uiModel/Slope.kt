package com.codewithmohsen.currencyratesapp.domain.uiModel

import com.codewithmohsen.R
import com.codewithmohsen.currencyratesapp.data.remote.toUi

sealed class Slope {

    object Positive: Slope() {
        override fun getIcon() =
            R.drawable.ui_increase_24
    }

    object Negative: Slope() {
        override fun getIcon(): Int =
            R.drawable.ui_decrease_24
    }

    object Zero: Slope() {
        override fun getIcon(): Int =
            R.drawable.ui_zero_24
    }

    object Init: Slope() {
        override fun getIcon(): Int =
            0

    }

    abstract fun getIcon(): Int

    companion object {
        @JvmStatic
        fun getSlope(newValue: Double, oldValue: Double?): Slope {
            return if (oldValue == null) Slope.Init
            else if (newValue > oldValue) Slope.Positive
            else if (newValue < oldValue) Slope.Negative
            else Slope.Zero
        }
    }
}