package com.codewithmohsen.currencyratesapp.domain.uiModel

import com.codewithmohsen.currencyratesapp.core.domain.UiData
import com.codewithmohsen.currencyratesapp.data.local.DalCurrencyRate
import com.codewithmohsen.currencyratesapp.data.local.DalCurrencyRateWithRates
import timber.log.Timber

data class UiCurrencyRates(
    val uiRates: List<UiRate>,
    val lastUpdate: String
) : UiData {
    companion object {
        @JvmStatic
        fun update(oldValue: UiCurrencyRates?, newValue: UiCurrencyRates?): UiCurrencyRates? {
            Timber.d("oldValue: $oldValue")
            Timber.d("newValue: $newValue")
            if (newValue == null) return oldValue

            val oldValueMap = mutableMapOf<ExchangeSymbol, UiRate>()
            oldValue?.uiRates?.forEach { oldValueMap[it.symbol] = it }

            return newValue.copy(uiRates = newValue.uiRates.map { rate ->
                UiRate.update(
                    oldValue = oldValueMap[rate.symbol],
                    newValue = rate
                )
            })
        }
    }
}

fun UiCurrencyRates.toDal(): DalCurrencyRateWithRates =
    DalCurrencyRateWithRates(
        dalCurrencyRate = DalCurrencyRate(lastUpdate = this.lastUpdate),
        rates = uiRates.map { it.toDal() })