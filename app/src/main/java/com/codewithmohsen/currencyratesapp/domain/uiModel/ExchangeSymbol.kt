package com.codewithmohsen.currencyratesapp.domain.uiModel

import com.codewithmohsen.R

enum class ExchangeSymbol {
    AUDCAD {
        override fun getText(): String =
            "AUD/CAD"

        override fun getIcon(): Int =
            R.drawable.audcad
    },
    EURUSD {
        override fun getText(): String =
            "EUR/USD"

        override fun getIcon(): Int =
            R.drawable.eurusd
    },
    GBPJPY {
        override fun getText(): String =
            "GBP/JPY"

        override fun getIcon(): Int =
            R.drawable.gbpjpy
    },
    JPYAED {
        override fun getText(): String =
            "JPY/AED"

        override fun getIcon(): Int =
            R.drawable.jpyaed
    },
    JPYSEK {
        override fun getText(): String =
            "JPY/SEK"

        override fun getIcon(): Int =
            R.drawable.jpysek
    },
    USDCAD {
        override fun getText(): String =
            "USD/CAD"

        override fun getIcon(): Int =
            R.drawable.usdcad
    },
    USDGBP {
        override fun getText(): String =
            "USD/GBP"

        override fun getIcon(): Int =
            R.drawable.usdgbp
    };

    abstract fun getText(): String
    abstract fun getIcon(): Int
}