package com.codewithmohsen.currencyratesapp.domain.repository

import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates

interface CurrencyRepository {
    fun getCurrency(): UiStateFlow<UiCurrencyRates>
}