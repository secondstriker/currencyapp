package com.codewithmohsen.currencyratesapp.domain.usecase

import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.domain.repository.CurrencyRepository
import javax.inject.Inject

class LoadRatesUseCase @Inject constructor(
    private val repository: CurrencyRepository
) {
    operator fun invoke(): UiStateFlow<UiCurrencyRates> =
        repository.getCurrency()
}