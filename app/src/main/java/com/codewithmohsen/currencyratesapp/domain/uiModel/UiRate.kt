package com.codewithmohsen.currencyratesapp.domain.uiModel

import com.codewithmohsen.currencyratesapp.data.local.DalRate

data class UiRate(
    val price: Double,
    val symbol: ExchangeSymbol,
    val slope: Slope
) {
    companion object {
        @JvmStatic
        fun update(newValue: UiRate, oldValue: UiRate?): UiRate =
            newValue.copy(
                slope = Slope.getSlope(
                    newValue = newValue.price,
                    oldValue = oldValue?.price
                )
            )
    }
}
fun UiRate.toDal(): DalRate =
    DalRate(price = this.price, symbol = this.symbol.name)