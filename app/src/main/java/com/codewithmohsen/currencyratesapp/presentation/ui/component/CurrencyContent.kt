package com.codewithmohsen.currencyratesapp.presentation.ui.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.codewithmohsen.R
import com.codewithmohsen.currencyratesapp.domain.uiModel.ExchangeSymbol
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiRate
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme
import com.codewithmohsen.currencyratesapp.core.presentation.theme.GreyText

@Composable
fun CurrencyContent(
    modifier: Modifier = Modifier,
    currencyRates: UiCurrencyRates?
) {

    Column(
        verticalArrangement = Arrangement.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = Color.White)
            .padding(all = 24.dp)
    ) {

        Text(
            text = stringResource(R.string.rates),
            style = MaterialTheme.typography.titleLarge,
        )

        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(16.dp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 24.dp)
                .weight(1f)
        ) {
            items(
                items = currencyRates?.uiRates ?: emptyList(),
                itemContent = { rate ->
                    RateItem(rate = rate)
                }
            )
        }

        Text(
            text = stringResource(id = R.string.last_update, currencyRates?.lastUpdate ?: ""),
            style = MaterialTheme.typography.labelMedium,
            color = GreyText,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .align(CenterHorizontally)
        )
    }
}


@Preview(showBackground = true)
@Composable
fun PreviewCurrencyScreen() {
    val currencyRates = UiCurrencyRates(
        uiRates = listOf(
            UiRate(1.554343, ExchangeSymbol.AUDCAD, Slope.Positive),
            UiRate(1.425567, ExchangeSymbol.EURUSD, Slope.Negative)
        ),
        lastUpdate = "last update"
    )
    CurrencyRatesAppTheme {
        CurrencyContent(
            currencyRates = currencyRates
        )
    }
}