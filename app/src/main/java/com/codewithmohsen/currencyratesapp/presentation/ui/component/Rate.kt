package com.codewithmohsen.currencyratesapp.presentation.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme
import com.codewithmohsen.currencyratesapp.core.presentation.theme.Red
import com.codewithmohsen.currencyratesapp.core.presentation.theme.Green
import com.codewithmohsen.currencyratesapp.utils.roundToString

@Composable
fun Rate(
    modifier: Modifier = Modifier,
    rate: Double,
    slope: Slope
) {
    val color = when(slope) {
        Slope.Negative -> Red
        Slope.Positive -> Green
        Slope.Init -> Color.Black
        Slope.Zero -> Color.Black
    }
    Row(modifier = modifier) {
        Text(
            modifier = Modifier.padding(end = 2.dp),
            text = rate.roundToString(4),
            style = MaterialTheme.typography.bodyMedium,
            color = color,
        )
        if(slope != Slope.Init)
            Image(
                painter = rememberVectorPainter(image = ImageVector.vectorResource(id = slope.getIcon())),
                contentDescription = null
            )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewRate() {
    CurrencyRatesAppTheme {
        Column() {
            Rate(
                rate = 1.54346346,
                slope = Slope.Init
            )

            Rate(
                rate = 1.555555,
                slope = Slope.Positive
            )

            Rate(
                rate = 1.3333333,
                slope = Slope.Negative
            )

            Rate(
                rate = 1.3333333,
                slope = Slope.Zero
            )
        }
    }
}