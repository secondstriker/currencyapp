package com.codewithmohsen.currencyratesapp.presentation.ui.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.codewithmohsen.currencyratesapp.presentation.ui.component.CurrencyContent
import com.codewithmohsen.currencyratesapp.utils.mapError
import com.codewithmohsen.currencyratesapp.presentation.vm.CurrencyViewModel
import com.codewithmohsen.currencyratesapp.core.presentation.compose.Loading


@Composable
fun CurrencyScreen(
    viewModel: CurrencyViewModel
) {
    val state by viewModel.uiState.collectAsStateWithLifecycle()
    val snackBarHostState: SnackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackBarHostState) }
    ) {

        if (state.loading)
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it),
                contentAlignment = Alignment.Center
            ) {
                Loading()
            }
        else
            CurrencyContent(
                modifier = Modifier.padding(it),
                currencyRates = state.data
            )

        state.throwable?.let { throwable ->
            val error = LocalContext.current.mapError(throwable)
            LaunchedEffect(Unit) {
                snackBarHostState.showSnackbar(error)
            }
        }
    }
}