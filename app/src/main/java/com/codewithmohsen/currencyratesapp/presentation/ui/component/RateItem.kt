package com.codewithmohsen.currencyratesapp.presentation.ui.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.codewithmohsen.currencyratesapp.domain.uiModel.ExchangeSymbol
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiRate
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme
import com.codewithmohsen.currencyratesapp.core.presentation.theme.Grey


@Composable
fun RateItem(
    modifier: Modifier = Modifier,
    rate: UiRate
) {

    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(
                color = Grey,
                shape = RoundedCornerShape(16.dp)
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Image(
            modifier = Modifier
                .padding(start = 16.dp, top = 13.dp, bottom = 13.dp),
            painter =
            painterResource(id = rate.symbol.getIcon()),
            contentDescription = null
        )

        Text(
            text = rate.symbol.getText(),
            style = MaterialTheme.typography.bodyLarge,
            modifier = Modifier
                .weight(1f)
                .padding(start = 8.dp)
        )

        Rate(
            rate = rate.price,
            slope = rate.slope,
            modifier = Modifier.padding(end = 27.dp)
        )
    }
}

@Preview(showBackground = false)
@Composable
fun PreviewRateItem() {
    CurrencyRatesAppTheme {
        RateItem(
            rate = UiRate(price = 1.4463634634, symbol = ExchangeSymbol.EURUSD, slope = Slope.Positive),
        )
    }
}