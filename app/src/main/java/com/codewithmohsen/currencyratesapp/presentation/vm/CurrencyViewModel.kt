package com.codewithmohsen.currencyratesapp.presentation.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.codewithmohsen.currencyratesapp.core.domain.UiState
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.domain.usecase.LoadRatesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CurrencyViewModel @Inject constructor(
    private val useCase: LoadRatesUseCase
): ViewModel() {

    private val _uiState: MutableStateFlow<UiState<UiCurrencyRates>> = MutableStateFlow(UiState())
    val uiState = _uiState.asStateFlow()

    init {
        loadRates()
    }

    private fun loadRates() =
        viewModelScope.launch {
            while (true) {
                useCase().collect {
                    _uiState.emit(it)
                }
                delay(DELAY)
            }
        }


    companion object {
        private const val DELAY = 12000L
    }
}