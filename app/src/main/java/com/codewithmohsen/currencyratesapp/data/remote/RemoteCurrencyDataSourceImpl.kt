package com.codewithmohsen.currencyratesapp.data.remote

import com.codewithmohsen.currencyratesapp.core.data.remote.toUiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RemoteCurrencyDataSourceImpl @Inject constructor(
    private val service: RatesService
) : RemoteCurrencyDataSource {
    override fun getCurrency(): UiStateFlow<UiCurrencyRates> =
        flow {
            emit(service.loadRates())
        }.toUiState(mapper = { dto -> dto.toUi() })
}