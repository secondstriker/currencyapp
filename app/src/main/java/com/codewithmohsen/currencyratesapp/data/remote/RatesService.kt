package com.codewithmohsen.currencyratesapp.data.remote

import retrofit2.http.GET

interface RatesService {
    @GET(value = "code-challenge/index.php")
    suspend fun loadRates(): CurrencyRates
}