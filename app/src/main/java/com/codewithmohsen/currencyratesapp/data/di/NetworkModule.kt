package com.codewithmohsen.currencyratesapp.data.di

import com.codewithmohsen.currencyratesapp.data.remote.RatesService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideBookService(retrofit: Retrofit.Builder): RatesService =
        retrofit.build().create(RatesService::class.java)

}