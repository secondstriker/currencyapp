package com.codewithmohsen.currencyratesapp.data.remote


import com.codewithmohsen.currencyratesapp.core.data.remote.DTO
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.utils.DateUtils
import com.google.gson.annotations.SerializedName

data class CurrencyRates(
    @SerializedName("rates")
    val rates: List<Rate>?
): DTO
fun CurrencyRates.toUi(): UiCurrencyRates = UiCurrencyRates(
    uiRates = this.rates?.map { rate -> rate.toUi() } ?: emptyList(),
    lastUpdate = DateUtils.getLastUpdate()
)