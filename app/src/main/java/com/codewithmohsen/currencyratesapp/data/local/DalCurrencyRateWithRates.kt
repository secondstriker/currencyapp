package com.codewithmohsen.currencyratesapp.data.local

import androidx.room.Embedded
import androidx.room.Relation
import com.codewithmohsen.currencyratesapp.core.data.local.DAL
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates

data class DalCurrencyRateWithRates(
    @Embedded
    val dalCurrencyRate: DalCurrencyRate,
    @Relation(
        parentColumn = "id",
        entityColumn = "currencyRateId"
    )
    val rates: List<DalRate>
): DAL

fun DalCurrencyRateWithRates.toUi(): UiCurrencyRates =
    UiCurrencyRates(uiRates = this.rates.map { it.toUi() }, lastUpdate = this.dalCurrencyRate.lastUpdate)