package com.codewithmohsen.currencyratesapp.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.codewithmohsen.currencyratesapp.domain.uiModel.ExchangeSymbol
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiRate


@Entity
data class DalRate(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var price: Double,
    var symbol: String,
    var currencyRateId: Long = 0 // Foreign key referencing DalCurrencyRate
)

fun DalRate.toUi(): UiRate = UiRate(
    price = this.price,
    symbol = ExchangeSymbol.valueOf(this.symbol),
    slope = Slope.Init
)