package com.codewithmohsen.currencyratesapp.data.remote

import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.data.local.LocalCurrencyDataSource
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import com.codewithmohsen.currencyratesapp.domain.repository.CurrencyRepository
import com.codewithmohsen.currencyratesapp.domain.uiModel.toDal
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class CurrencyRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteCurrencyDataSource,
    private val localDataSource: LocalCurrencyDataSource
): CurrencyRepository {

    override fun getCurrency(): UiStateFlow<UiCurrencyRates> =
        remoteDataSource.getCurrency().combine(localDataSource.getCurrency()) { remote, local ->
            remote.data?.let { localDataSource.insert(it.toDal()) }
            remote.copy(data = UiCurrencyRates.update(oldValue = local.data, newValue = remote.data))
        }
}