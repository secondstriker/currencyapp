package com.codewithmohsen.currencyratesapp.data.di

import com.codewithmohsen.currencyratesapp.data.local.LocalCurrencyDataSource
import com.codewithmohsen.currencyratesapp.data.local.LocalCurrencyDataSourceImpl
import com.codewithmohsen.currencyratesapp.data.remote.RemoteCurrencyDataSource
import com.codewithmohsen.currencyratesapp.data.remote.RemoteCurrencyDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface CurrencyDataSourceModule {

    @Binds
    fun bindsRemoteCurrencyDataSource(impl: RemoteCurrencyDataSourceImpl): RemoteCurrencyDataSource
    @Binds
    fun bindsLocalCurrencyDataSource(impl: LocalCurrencyDataSourceImpl): LocalCurrencyDataSource
}