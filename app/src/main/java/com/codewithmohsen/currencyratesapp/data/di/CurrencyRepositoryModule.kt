package com.codewithmohsen.currencyratesapp.data.di

import com.codewithmohsen.currencyratesapp.domain.repository.CurrencyRepository
import com.codewithmohsen.currencyratesapp.data.remote.CurrencyRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
interface CurrencyRepositoryModule {
    @Binds
    fun provideCurrencyRepository(imp: CurrencyRepositoryImpl): CurrencyRepository
}