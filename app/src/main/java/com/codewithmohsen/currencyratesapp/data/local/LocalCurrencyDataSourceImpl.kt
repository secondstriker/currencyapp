package com.codewithmohsen.currencyratesapp.data.local

import com.codewithmohsen.currencyratesapp.core.data.local.di.CoroutinesScopesModule.ApplicationScope
import com.codewithmohsen.currencyratesapp.core.data.local.di.IoDispatcher
import com.codewithmohsen.currencyratesapp.core.data.local.toUiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class LocalCurrencyDataSourceImpl @Inject constructor(
    private val dao: CurrencyRateDao,
    @ApplicationScope private val scope: CoroutineScope,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : LocalCurrencyDataSource {

    override fun insert(dalCurrencyRateWithRates: DalCurrencyRateWithRates) {
        scope.launch(dispatcher) {
            dao.insert(dalCurrencyRateWithRates)
        }
    }

    override fun getCurrency(): UiStateFlow<UiCurrencyRates> =
        flow {
            emit(dao.getLastCurrencyRate())
        }.toUiState( mapper = { it.toUi() })
}