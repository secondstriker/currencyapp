package com.codewithmohsen.currencyratesapp.data.di

import com.codewithmohsen.currencyratesapp.core.data.local.AppDatabase
import com.codewithmohsen.currencyratesapp.data.local.CurrencyRateDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RateDaoModule {
    @Provides
    @Singleton
    fun provideRateDao(db: AppDatabase): CurrencyRateDao =
        db.dalRateDao()
}