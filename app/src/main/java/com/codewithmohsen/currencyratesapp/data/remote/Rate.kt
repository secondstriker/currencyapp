package com.codewithmohsen.currencyratesapp.data.remote


import com.codewithmohsen.currencyratesapp.domain.uiModel.ExchangeSymbol
import com.codewithmohsen.currencyratesapp.domain.uiModel.Slope
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiRate
import com.google.gson.annotations.SerializedName

data class Rate(
    @SerializedName("price")
    val price: Double,
    @SerializedName("symbol")
    val symbol: String?
)

fun Rate.toUi(): UiRate = UiRate(
    price = this.price,
    symbol = ExchangeSymbol.valueOf(this.symbol ?: ""),
    slope = Slope.Init
)