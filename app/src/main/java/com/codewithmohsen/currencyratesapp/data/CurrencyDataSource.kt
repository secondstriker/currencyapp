package com.codewithmohsen.currencyratesapp.data

import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import com.codewithmohsen.currencyratesapp.domain.uiModel.UiCurrencyRates

interface CurrencyDataSource {
    fun getCurrency(): UiStateFlow<UiCurrencyRates>
}