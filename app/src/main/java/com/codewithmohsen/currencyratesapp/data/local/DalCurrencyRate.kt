package com.codewithmohsen.currencyratesapp.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DalCurrencyRate(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var lastUpdate: String,
)