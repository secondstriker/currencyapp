package com.codewithmohsen.currencyratesapp.data.local

import com.codewithmohsen.currencyratesapp.data.CurrencyDataSource

interface LocalCurrencyDataSource : CurrencyDataSource {
    fun insert(dalCurrencyRateWithRates: DalCurrencyRateWithRates)
}