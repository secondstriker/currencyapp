package com.codewithmohsen.currencyratesapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import timber.log.Timber

@Dao
abstract class CurrencyRateDao {
    @Transaction
    @Query("SELECT * FROM DalCurrencyRate")
    abstract suspend fun getCurrencyRate(): List<DalCurrencyRateWithRates>

    suspend fun getLastCurrencyRate(): DalCurrencyRateWithRates =
        getCurrencyRate().last()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(dalCurrencyRate: DalCurrencyRate): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(vararg dalRate: DalRate): List<Long>

    suspend fun insert(dalCurrencyRateWithRates: DalCurrencyRateWithRates) {
        Timber.d("inserting $dalCurrencyRateWithRates")
        val id = insert(dalCurrencyRateWithRates.dalCurrencyRate)
        Timber.d("inserted with id $id")
        val ids = insert(*dalCurrencyRateWithRates.rates.map { it.copy(currencyRateId = id) }.toTypedArray())
        Timber.d("inserted with id $ids")
    }
}