package com.codewithmohsen.currencyratesapp.core.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.codewithmohsen.currencyratesapp.data.local.CurrencyRateDao
import com.codewithmohsen.currencyratesapp.data.local.DalCurrencyRate
import com.codewithmohsen.currencyratesapp.data.local.DalRate

@Database(entities = [DalRate::class, DalCurrencyRate::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dalRateDao(): CurrencyRateDao
}