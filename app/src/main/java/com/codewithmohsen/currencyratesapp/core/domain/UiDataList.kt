package com.codewithmohsen.currencyratesapp.core.domain

abstract class UiDataList<T: UiData>: ArrayList<T>(), UiData