package com.codewithmohsen.currencyratesapp.core.presentation.compose

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme

private const val MIN_ALPHA = 0.5F
private const val DELAY_MILLIS = 200

@Composable
fun Loading(
    modifier: Modifier = Modifier,
    dotColor: Color = MaterialTheme.colorScheme.primary,
) {
    val infiniteTransition = rememberInfiniteTransition()

    val alpha1 by infiniteTransition.animateAlpha(delay = DELAY_MILLIS * 2)
    val alpha2 by infiniteTransition.animateAlpha(delay = DELAY_MILLIS)
    val alpha3 by infiniteTransition.animateAlpha(delay = 0)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = modifier.padding(4.dp)
    ) {
        Dot(
            alpha = alpha1,
            color = dotColor
        )
        Spacer(modifier = Modifier.width(8.dp))
        Dot(
            alpha = alpha2,
            color = dotColor
        )
        Spacer(modifier = Modifier.width(8.dp))
        Dot(
            alpha = alpha3,
            color = dotColor
        )
    }
}

@Composable
private fun InfiniteTransition.animateAlpha(delay: Int) = animateFloat(
    initialValue = MIN_ALPHA,
    targetValue = MIN_ALPHA,
    animationSpec = infiniteRepeatable(
        animation = keyframes {
            durationMillis = DELAY_MILLIS * 4
            MIN_ALPHA at delay with LinearEasing
            1f at delay + DELAY_MILLIS with LinearEasing
            MIN_ALPHA at delay + DELAY_MILLIS * 2
        }
    )
)

@Composable
private fun Dot(
    alpha: Float,
    color: Color,
) = Spacer(
    modifier = Modifier
        .size(8.dp)
        .alpha(alpha)
        .background(
            color = color,
            shape = CircleShape
        )
)

@Preview()
@Composable
fun PreviewLoading() = CurrencyRatesAppTheme {
    Surface(
        modifier = Modifier.fillMaxWidth()
    ) {
        Loading()
    }
}
