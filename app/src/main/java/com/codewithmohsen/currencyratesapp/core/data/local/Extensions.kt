package com.codewithmohsen.currencyratesapp.core.data.local

import com.codewithmohsen.currencyratesapp.core.domain.UiData
import com.codewithmohsen.currencyratesapp.core.domain.UiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart


fun <DALType : DAL,
        UiDataType : UiData>
        Flow<DALType>.toUiState(mapper: (DALType) -> UiDataType): UiStateFlow<UiDataType> =
    this.map {
        UiState(mapper(it), false, null)
    }.catch {
        emit(UiState<UiDataType>(null, false, it.cause))
    }
