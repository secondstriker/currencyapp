package com.codewithmohsen.currencyratesapp.core.presentation.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDirection
import androidx.compose.ui.unit.sp
import com.codewithmohsen.R

private val Satoshi = FontFamily(
    Font(R.font.satoshi_regular, FontWeight.Normal),
    Font(R.font.satoshi_medium, FontWeight.Medium),
    Font(R.font.satoshi_bold, FontWeight.Bold),
)

val Typography = Typography(

    titleLarge = TextStyle(
        fontFamily = Satoshi,
        fontWeight = FontWeight.Bold,
        fontStyle = FontStyle.Normal,
        textDirection = TextDirection.Ltr,
        fontSize = 48.sp,
    ),
    bodyLarge = TextStyle(
        fontFamily = Satoshi,
        fontWeight = FontWeight.Bold,
        fontStyle = FontStyle.Normal,
        textDirection = TextDirection.Ltr,
        fontSize = 18.sp
    ),
    bodyMedium = TextStyle(
        fontFamily = Satoshi,
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        textDirection = TextDirection.Ltr,
        fontSize = 18.sp
    ),
    labelMedium = TextStyle(
        fontFamily = Satoshi,
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        textDirection = TextDirection.Ltr,
        fontSize = 12.sp
    )
)
