package com.codewithmohsen.currencyratesapp.core.domain

data class UiState<out T>(val `data`: T? = null, val loading: Boolean = true, val throwable: Throwable? = null)