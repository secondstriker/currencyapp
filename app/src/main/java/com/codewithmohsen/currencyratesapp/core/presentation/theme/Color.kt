package com.codewithmohsen.currencyratesapp.core.presentation.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val Green = Color(0xFF02cb16)
val Red = Color(0xFFfe2525)

val Grey = Color(0xFFf9f9f9)
val GreyText = Color(0xFFcccccc)