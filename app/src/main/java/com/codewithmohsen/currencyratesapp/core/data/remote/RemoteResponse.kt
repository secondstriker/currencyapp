package com.codewithmohsen.currencyratesapp.core.data.remote

data class RemoteResponse<out T>(val data: T, val succeed: Boolean, val message: String?)