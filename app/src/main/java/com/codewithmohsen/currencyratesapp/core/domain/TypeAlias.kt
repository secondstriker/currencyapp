package com.codewithmohsen.currencyratesapp.core.domain

import kotlinx.coroutines.flow.Flow

typealias UiStateFlow<T> = Flow<UiState<T>>