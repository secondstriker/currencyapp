package com.codewithmohsen.currencyratesapp.core.data.remote

abstract class DTOList<T: DTO>: ArrayList<T>(), DTO