package com.codewithmohsen.currencyratesapp.core.data.remote

data class ServerException(override val message: String): Exception()