package com.codewithmohsen.currencyratesapp.core.data.remote

import com.codewithmohsen.currencyratesapp.core.domain.UiData
import com.codewithmohsen.currencyratesapp.core.domain.UiState
import com.codewithmohsen.currencyratesapp.core.domain.UiStateFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

fun <DTOType : DTO,
        UiDataType : UiData>
        Flow<RemoteResponse<DTOType>>.remoteResponseToUiState(mapper: (DTOType) -> UiDataType): UiStateFlow<UiDataType> =
    this.map {
        when {
            !it.succeed -> {
                UiState<UiDataType>(null, false, ServerException(it.message!!))
            }
            else -> {
                UiState(mapper(it.data), false, null)
            }
        }
    }.onStart {
        emit(UiState<UiDataType>(null, true, null))
    }.catch {
        emit(UiState<UiDataType>(null, false, it.cause))
    }

fun <DTOType : DTO,
        UiDataType : UiData>
        Flow<DTOType>.toUiState(mapper: (DTOType) -> UiDataType): UiStateFlow<UiDataType> =
    this.map {
        UiState(mapper(it), false, null)
    }.onStart {
        emit(UiState<UiDataType>(null, true, null))
    }.catch {
        emit(UiState<UiDataType>(null, false, it.cause))
    }
