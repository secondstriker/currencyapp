package com.codewithmohsen.currencyratesapp.utils

import android.content.Context
import com.codewithmohsen.R
import com.codewithmohsen.currencyratesapp.core.data.remote.ServerException
import retrofit2.HttpException

fun Context.mapError(throwable: Throwable): String {
    return when (throwable) {
        is ServerException -> throwable.message
        is HttpException -> this.getString(R.string.connectivity_error)
        else -> this.getString(R.string.invalid_data)
    }
}