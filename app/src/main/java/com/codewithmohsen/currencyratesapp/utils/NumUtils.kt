package com.codewithmohsen.currencyratesapp.utils

import kotlin.math.pow
import kotlin.math.roundToInt

fun Double.roundTo(numFractionDigits: Int): Double {
    val factor = 10.0.pow(numFractionDigits.toDouble())
    return (this * factor).roundToInt() / factor
}

fun Double.roundToString(numFractionDigits: Int): String {
    val roundTo = this.roundTo(numFractionDigits).toString()
    return roundTo.padEnd(6, '0')
}