package com.codewithmohsen.currencyratesapp.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun getLastUpdate(): String {

        val calendar: Calendar = Calendar.getInstance()
        val format = SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", Locale.US)
        return format.format(calendar.time)
    }
}