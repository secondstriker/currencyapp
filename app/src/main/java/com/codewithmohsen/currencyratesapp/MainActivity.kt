package com.codewithmohsen.currencyratesapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import com.codewithmohsen.currencyratesapp.presentation.ui.screen.CurrencyScreen
import com.codewithmohsen.currencyratesapp.core.presentation.theme.CurrencyRatesAppTheme
import com.codewithmohsen.currencyratesapp.presentation.vm.CurrencyViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: CurrencyViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CurrencyRatesAppTheme {
                CurrencyScreen(viewModel = viewModel)
            }
        }
    }
}